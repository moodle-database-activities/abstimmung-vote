# Vote

Vote is a preset for the Moodle activity database.

## Demo

https://fdagner.de/moodle/mod/data/view.php?d=7&perpage=1000

## Getting started

Download the source code and zip the files WITHOUT parent folder. Create a "Database" activity in Moodle and then upload the ZIP file in the "Presets" tab under "Import".

There are two options in the footer of the list view: 

1. Manual sorting and display of null values: the field names must be specified.

<code>// let uniqueNums = Object.keys(answer);
let uniqueNums = ["😀","😐","🙁","😎"];</code>


2.  Sort by user submission without displaying zero values. Field names do not have to be entered.

<code>let uniqueNums = Object.keys(answer);
// let uniqueNums = [];</code>




## Language Support

The preset is available in German. 

## Description

Visually appealing feedback and polls can be created.

## License

https://creativecommons.org/licenses/by/4.0/

## Screenshots

<img width="400" alt="single view" src="/screenshots/listenansicht.png">




